module "container" {
  source          = "../../"
  ecs_container_name  = "name"
  ecs_container_image = "cloudposse/geodesic"

  ecs_container_environment = [
    {
      name  = "string_var"
      value = "123"
    },
    {
      name  = "another_string_var"
      value = "true"
    },
    {
      name  = "yet_another_string_var"
      value = "false"
    }
  ]
}

output "json" {
  description = "Container definition in JSON format"
  value       = module.container.container_definitions_list
}
