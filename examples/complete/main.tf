provider "aws" {
  region = var.region
}

module "container" {
  source                       = "../.."
  ecs_container_name               = var.container_name
  ecs_container_image              = var.container_image
  ecs_container_memory             = var.container_memory
  ecs_container_memory_reservation = var.container_memory_reservation
  ecs_container_cpu                = var.container_cpu
  ecs_container_essential                    = var.essential
  ecs_container_readonly_root_filesystem     = var.readonly_root_filesystem
  ecs_container_environment                  = var.environment
  ecs_container_port_mappings                = var.port_mappings
  ecs_container_log_configuration            = var.log_configuration
}
