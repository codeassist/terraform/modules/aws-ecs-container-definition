module "container" {
  source          = "../../"
  ecs_container_name  = "name"
  ecs_container_image = "cloudposse/geodesic"

  ecs_container_environment = [
    {
      name  = "string_var"
      value = "I am a string"
    },
    {
      name  = "true_boolean_var"
      value = true
    },
    {
      name  = "false_boolean_var"
      value = false
    },
    {
      name  = "integer_var"
      value = 42
    }
  ]

  ecs_container_port_mappings = [
    {
      containerPort = 8080
      hostPort      = 80
      protocol      = "tcp"
    },
    {
      containerPort = 8081
      hostPort      = 443
      protocol      = "udp"
    }
  ]
}

output "json" {
  description = "Container definition in JSON format"
  value       = module.container.container_definitions_list
}
