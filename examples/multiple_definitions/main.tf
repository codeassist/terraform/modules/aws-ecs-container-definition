module "first_container" {
  source          = "../../"
  ecs_container_name  = "name"
  ecs_container_image = "cloudposse/geodesic"

  ecs_container_port_mappings = [
    {
      containerPort = 8080
      hostPort      = 80
      protocol      = "tcp"
    },
    {
      containerPort = 8081
      hostPort      = 443
      protocol      = "udp"
    }
  ]
}

module "second_container" {
  source          = "../../"
  ecs_container_name  = "name2"
  ecs_container_image = "cloudposse/geodesic"

  ecs_container_port_mappings = [
    {
      containerPort = 8080
      hostPort      = 8080
      protocol      = "tcp"
    },
    {
      containerPort = 8081
      hostPort      = 444
      protocol      = "udp"
    }
  ]
}

output "first_container_json" {
  description = "Container definition in JSON format"
  value       = module.first_container.container_definitions_list
}

output "second_container_json" {
  description = "Container definition in JSON format"
  value       = module.second_container.container_definitions_list
}

resource "aws_ecs_task_definition" "task" {
  family                = "foo"
  container_definitions = "[${module.first_container.container_definition},${module.second_container.container_definition}]"
}
