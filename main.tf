locals {
  # Sort environment variables so terraform will not try to recreate on each plan/apply
  env_vars             = var.ecs_container_environment != null ? var.ecs_container_environment : []
  env_vars_keys        = [for m in local.env_vars : lookup(m, "name")]
  env_vars_values      = [for m in local.env_vars : lookup(m, "value")]
  env_vars_as_map      = zipmap(local.env_vars_keys, local.env_vars_values)
  sorted_env_vars_keys = sort(local.env_vars_keys)

  sorted_environment_vars = [
    for key in local.sorted_env_vars_keys :
    {
      name  = key
      value = lookup(local.env_vars_as_map, key)
    }
  ]

  # This strange-looking variable is needed because terraform (currently) does not support explicit `null` in ternary
  # operator, so this does not work:
  #   final_environment_vars = length(local.sorted_environment_vars) > 0 ? local.sorted_environment_vars : null
  # See more at:
  #    * https://www.terraform.io/docs/configuration/expressions.html#null
  null_value             = var.ecs_container_environment == null ? var.ecs_container_environment : null
  final_environment_vars = length(local.sorted_environment_vars) > 0 ? local.sorted_environment_vars : local.null_value

  # See available options at:
  #   * https://docs.aws.amazon.com/en_us/AmazonECS/latest/APIReference/API_ContainerDefinition.html
  container_definition = {
    name                   = var.ecs_container_name
    image                  = var.ecs_container_image
    essential              = var.ecs_container_essential
    entryPoint             = var.ecs_container_entrypoint
    command                = var.ecs_container_command
    workingDirectory       = var.ecs_container_working_directory
    readonlyRootFilesystem = var.ecs_container_readonly_root_filesystem
    mountPoints            = var.ecs_container_mount_points
    dnsServers             = var.ecs_container_dns_servers
    ulimits                = var.ecs_container_ulimits
    repositoryCredentials  = var.ecs_container_repository_credentials
    links                  = var.ecs_container_links
    volumesFrom            = var.ecs_container_volumes_from
    user                   = var.ecs_container_user
    dependsOn              = var.ecs_container_depends_on
    privileged             = var.ecs_container_privileged
    portMappings           = var.ecs_container_port_mappings
    healthCheck            = var.ecs_container_healthcheck
    firelensConfiguration  = var.ecs_container_firelens_configuration
    logConfiguration       = var.ecs_container_log_configuration
    memory                 = var.ecs_container_memory
    memoryReservation      = var.ecs_container_memory_reservation
    cpu                    = var.ecs_container_cpu
    environment            = local.final_environment_vars
    secrets                = var.ecs_container_secrets
    dockerLabels           = var.ecs_container_labels
    startTimeout           = var.ecs_container_start_timeout
    stopTimeout            = var.ecs_container_stop_timeout
    systemControls         = var.ecs_container_system_controls
  }
}
