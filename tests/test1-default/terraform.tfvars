# ----------------------------
# Docker Container parameters
# ----------------------------
# (Required) The name of the container. Up to 255 characters ([a-z], [A-Z], [0-9], -, _ allowed).
ecs_container_name = "test1"
# (Required) The image used to start the container. Images in the Docker Hub registry available by default.
ecs_container_image = "docker:stable"
# The amount of memory (in MiB) to allow the container to use.
# This is a hard limit, if the container attempts to exceed the `ecs_container_memory`, the container is killed!
# This field is optional for FARGATE launch type and the total amount of container_memory of all containers in
# a task will need to be lower than the task memory value.
ecs_container_memory = 256
# The amount of memory (in MiB) to reserve for the container. If container needs to exceed this threshold,
# it can do so up to the set `ecs_container_memory hard limit.
ecs_container_memory_reservation = 128
# The port mappings to configure for the container. This is a list of maps. Each map should contain
# 'containerPort', 'hostPort', and 'protocol', where 'protocol' is one of 'tcp' or 'udp'.
# If using containers in a task with the `awsvpc` or `host` network mode, the `hostPort` can either be left
# blank or set to the same value as the `containerPort`.
ecs_container_port_mappings = [
  {
    containerPort = 80
    hostPort      = 80
    protocol      = "tcp"
  }
]
# A map containing command (string), timeout, interval (duration in seconds), retries (1-10, number of times
# to retry before marking container unhealthy), and startPeriod (0-300, optional grace period to wait,
# in seconds, before failed healthchecks count toward retries). See:
#   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_HealthCheck.html
ecs_container_healthcheck = null
# The number of cpu units to reserve for the container.
# This is optional for tasks using FARGATE launch type and the total amount of `ecs_container_cpu` of all
# containers in a task will need to be lower than the task-level cpu value
ecs_container_cpu = 256
# Determines whether all other containers in a task are stopped, if this container fails or stops for any reason.
ecs_container_essential = true
# The entry point that is passed to the container.
ecs_container_entrypoint = []
# The command that is passed to the container.
ecs_container_command = []
# The working directory to run commands inside the container.
ecs_container_working_directory = null
# The environment variables to pass to the container. This is a list of maps.
ecs_container_environment = []
# The secrets to pass to the container. This is a list of maps.
ecs_container_secrets = []
# Determines whether a container is given read-only access to its root filesystem.
ecs_container_readonly_root_filesystem = false
# Log configuration options to send to a custom log driver for the container. For more details, see:
#   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_LogConfiguration.html
ecs_container_log_configuration = null
# The FireLens configuration for the container. This is used to specify and configure a log router for container
# logs. For more details, see:
#   * https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_FirelensConfiguration.html
ecs_container_firelens_configuration = null
# Container mount points. This is a list of maps, where each map should contain a `containerPath` and
# `sourceVolume`
ecs_container_mount_points = []
# Container DNS servers. This is a list of strings specifying the IP addresses of the DNS servers.
ecs_container_dns_servers = []
# Container ulimit settings. This is a list of maps, where each map should contain 'name', 'hardLimit' and
# 'softLimit'.
ecs_container_ulimits = []
# Container repository credentials; required when using a private repo. This map currently supports a single key:
# 'credentialsParameter', which should be the ARN of a Secrets Manager's secret holding the credentials
# NOTE(!): must be `null`, otherwise an error `missing required field, RegisterTaskDefinitionInput.ContainerDefinitions[0].RepositoryCredentials.CredentialsParameter`
# may occur.
ecs_container_repository_credentials = null
# A list of VolumesFrom maps which contain 'sourceContainer' (name of the container that has the volumes
# to mount) and 'readOnly' (whether the container can write to the volume).
ecs_container_volumes_from = []
# List of container names this container can communicate with without port mappings.
ecs_container_links = []
# The user to run as inside the container. Can be any of these formats:
#   * user,
#   * user:group,
#   * uid,
#   * uid:gid,
#   * user:gid,
#   * uid:group.
ecs_container_user = null
# The dependencies defined for container startup and shutdown. A container can contain multiple dependencies.
# When a dependency is defined for container startup, for container shutdown it is reversed."
ecs_container_depends_on = []
# The configuration options to send to the `docker_labels`.
ecs_container_labels = {}
# Time duration (in seconds) to wait before giving up on resolving dependencies for a container.
ecs_container_start_timeout = 30
# Time duration (in seconds) to wait before the container is forcefully killed if it doesn't exit normally on
# its own.
ecs_container_stop_timeout = 30
# When this variable is `true`, the container is given elevated privileges on the host container instance
# (similar to the root user).
# NOTE(!): this parameter is not supported for Windows containers or tasks using the FARGATE launch type.
ecs_container_privileged = false
# A list of namespaced kernel parameters to set in the container, mapping to the --sysctl option to docker run.
# This is a list of maps:
# [
#   {
#     namespace = "",
#     value = ""
#   }
# ]
ecs_container_system_controls = []
