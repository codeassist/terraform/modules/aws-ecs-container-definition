# ----------------------------
# Docker Container parameters
# ----------------------------
variable "ecs_container_name" {
  description = "(Required) The name of the container. Up to 255 characters ([a-z], [A-Z], [0-9], -, _ allowed)."
  type        = string
}

variable "ecs_container_image" {
  description = "(Required) The image used to start the container. Images in the Docker Hub registry available by default."
  type        = string
}

variable "ecs_container_memory" {
  description = "The amount of memory (in MiB) to allow the container to use. This is a hard limit, if the container attempts to exceed the container_memory, the container is killed."
  # This field is optional for FARGATE launch type and the total amount of `ecs_container_memory` of all containers in
  # a task will need to be lower than the task memory value.
  type    = number
  default = 256
}

variable "ecs_container_memory_reservation" {
  description = "The amount of memory (in MiB) to reserve for the container."
  # If container needs to exceed this threshold, it can do so up to the set `ecs_container_memory` hard limit.
  type    = number
  default = 128
}

variable "ecs_container_port_mappings" {
  description = "The port mappings to configure for the container."
  # This is a list of maps. Each map should contain 'containerPort', 'hostPort', and 'protocol', where 'protocol' is
  # one of 'tcp' or 'udp'.
  # If using containers in a task with the `awsvpc` or `host` network mode, the `hostPort` can either be left blank or
  # set to the same value as the `containerPort`.
  type = list(object({
    containerPort = number
    hostPort      = number
    protocol      = string
  }))
  default = []
}

variable "ecs_container_healthcheck" {
  description = "A map containing command (string), timeout, interval (duration in seconds), retries (1-10, number of times to retry before marking container unhealthy), and startPeriod (0-300, optional grace period to wait, in seconds, before failed healthchecks count toward retries)."
  # https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_HealthCheck.html
  type = object({
    command     = list(string)
    retries     = number
    timeout     = number
    interval    = number
    startPeriod = number
  })
  default = null
}

variable "ecs_container_cpu" {
  description = "The number of cpu units to reserve for the container."
  # This is optional for tasks using FARGATE launch type and the total amount of container_cpu of all containers in
  # a task will need to be lower than the task-level cpu value
  type    = number
  default = 256
}

variable "ecs_container_essential" {
  description = "Determines whether all other containers in a task are stopped, if this container fails or stops for any reason."
  type        = bool
  default     = true
}

variable "ecs_container_entrypoint" {
  description = "The entry point that is passed to the container."
  type        = list(string)
  default     = []
}

variable "ecs_container_command" {
  description = "The command that is passed to the container."
  type        = list(string)
  default     = []
}

variable "ecs_container_working_directory" {
  description = "The working directory to run commands inside the container."
  type        = string
  default     = null
}

variable "ecs_container_environment" {
  description = "The environment variables to pass to the container. This is a list of maps."
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "ecs_container_secrets" {
  description = "The secrets to pass to the container. This is a list of maps."
  type = list(object({
    name      = string
    valueFrom = string
  }))
  default = []
}

variable "ecs_container_readonly_root_filesystem" {
  description = "Determines whether a container is given read-only access to its root filesystem."
  type        = bool
  default     = false
}

variable "ecs_container_log_configuration" {
  description = "Log configuration options to send to a custom log driver for the container."
  # For more details, see https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_LogConfiguration.html
  type = object({
    logDriver = string
    options   = map(string)
    secretOptions = list(object({
      name      = string
      valueFrom = string
    }))
  })
  default = null
}

variable "ecs_container_firelens_configuration" {
  description = "The FireLens configuration for the container. This is used to specify and configure a log router for container logs."
  # For more details, see https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_FirelensConfiguration.html
  type = object({
    type    = string
    options = map(string)
  })
  default = null
}

variable "ecs_container_mount_points" {
  description = "Container mount points."
  # This is a list of maps, where each map should contain a `containerPath` and `sourceVolume`
  type = list(object({
    # The path on the container to mount the volume at.
    containerPath = string
    # The name of the volume to mount.
    sourceVolume = string
    # If this value is "true", the container has read-only access to the volume. If this value is "false", then
    # the container can write to the volume.
    readOnly = bool
  }))
  default = []
}

variable "ecs_container_dns_servers" {
  description = "Container DNS servers. This is a list of strings specifying the IP addresses of the DNS servers."
  type        = list(string)
  default     = []
}

variable "ecs_container_ulimits" {
  description = "Container ulimit settings. This is a list of maps, where each map should contain 'name', 'hardLimit' and 'softLimit'."
  type = list(object({
    name      = string
    hardLimit = number
    softLimit = number
  }))
  default = []
}

variable "ecs_container_repository_credentials" {
  description = "Container repository credentials; required when using a private repo."
  # This map currently supports a single key: 'credentialsParameter', which should be the ARN of a Secrets Manager's
  # secret holding the credentials.
  type = map(string)
  # Note(!): must be `null`, otherwise an error `missing required field, RegisterTaskDefinitionInput.ContainerDefinitions[0].RepositoryCredentials.CredentialsParameter`
  # may occur.
  default = null
}

variable "ecs_container_volumes_from" {
  description = "A list of VolumesFrom maps which contain 'sourceContainer' (name of the container that has the volumes to mount) and 'readOnly' (whether the container can write to the volume)."
  type = list(object({
    sourceContainer = string
    readOnly        = bool
  }))
  default = []
}

variable "ecs_container_links" {
  description = "List of container names this container can communicate with without port mappings."
  type        = list(string)
  default     = []
}

variable "ecs_container_user" {
  description = "The user to run as inside the container."
  # Can be any of these formats:
  #   * user,
  #   * user:group,
  #   * uid,
  #   * uid:gid,
  #   * user:gid,
  #   * uid:group.
  type    = string
  default = null
}

variable "ecs_container_depends_on" {
  description = "The dependencies defined for container startup and shutdown. A container can contain multiple dependencies. When a dependency is defined for container startup, for container shutdown it is reversed."
  type = list(object({
    # (Required) The container name that must meet the specified condition.
    containerName = string
    # (Required) The dependency condition of the container. The following are the available conditions and their
    # behavior:
    #   * START    - This condition emulates the behavior of links and volumes today. It validates that a dependent
    #                container is started before permitting other containers to start.
    #   * COMPLETE - This condition validates that a dependent container runs to completion (exits) before permitting
    #                other containers to start. This can be useful for nonessential containers that run a script and
    #                then exit.
    #   * SUCCESS  - This condition is the same as COMPLETE, but it also requires that the container exits with a zero
    #                status.
    #   * HEALTHY  - This condition validates that the dependent container passes its Docker healthcheck before
    #                permitting other containers to start. This requires that the dependent container has health
    #                checks configured. This condition is confirmed only at task startup.
    condition = string
  }))
  default = []
}

variable "ecs_container_labels" {
  description = "The configuration options to send to the `docker_labels`."
  type        = map(string)
  default     = {}
}

variable "ecs_container_start_timeout" {
  description = "Time duration (in seconds) to wait before giving up on resolving dependencies for a container."
  type        = number
  default     = 30
}

variable "ecs_container_stop_timeout" {
  description = "Time duration (in seconds) to wait before the container is forcefully killed if it doesn't exit normally on its own."
  type        = number
  default     = 30
}

variable "ecs_container_privileged" {
  description = "When this variable is `true`, the container is given elevated privileges on the host container instance (similar to the root user)."
  # This parameter is not supported for Windows containers or tasks using the FARGATE launch type.
  type    = bool
  default = false
}

variable "ecs_container_system_controls" {
  description = "A list of namespaced kernel parameters to set in the container, mapping to the --sysctl option to docker run."
  # This is a list of maps: { namespace = "", value = ""}
  type    = list(map(string))
  default = []
}
