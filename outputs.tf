output "json_list_container_definitions" {
  description = "JSON encoded list of container definitions for use with other terraform resources such as `aws_ecs_task_definition`."
  value       = jsonencode([local.container_definition])
}

output "json_map_container_definition" {
  description = "JSON encoded container definition map."
  value       = jsonencode(local.container_definition)
}
